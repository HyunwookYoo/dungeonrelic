// Fill out your copyright notice in the Description page of Project Settings.


#include "AnimInstances/HanAnimInstance.h"
#include "Characters/HanPlayerCharacter.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/KismetMathLibrary.h"

void UHanAnimInstance::NativeInitializeAnimation()
{
	Super::NativeInitializeAnimation();

	OwningCharacter = Cast<AHanPlayerCharacter>(TryGetPawnOwner());
	if (OwningCharacter)
	{
		MovementComponent = OwningCharacter->GetCharacterMovement();
	}
}

void UHanAnimInstance::NativeUpdateAnimation(float DeltaSeconds)
{
	Super::NativeUpdateAnimation(DeltaSeconds);

	if (MovementComponent)
	{
		GroundSpeed = UKismetMathLibrary::VSizeXY(MovementComponent->Velocity);
		bIsFalling = MovementComponent->IsFalling();
		bShouldMove = MovementComponent->GetCurrentAcceleration() != FVector::ZeroVector && GroundSpeed > 3.f;

		float YawBeforeScale = UKismetMathLibrary::NormalizedDeltaRotator(RotationLastTick, OwningCharacter->GetActorRotation()).Yaw;
		float YawTarget = (YawBeforeScale / DeltaSeconds) / 7.f;
		YawDelta = UKismetMathLibrary::FInterpTo(YawDelta, YawTarget, DeltaSeconds, 6.f);
		RotationLastTick = OwningCharacter->GetActorRotation();
	}
}

void UHanAnimInstance::AnimNotify_SaveAttack()
{
	// UE_LOG(LogTemp, Warning, TEXT("Save attack called"))
	if (OwningCharacter)
	{
		OwningCharacter->ComboAttackSave();
	}
}

void UHanAnimInstance::AnimNotify_ResetCombo()
{
	// UE_LOG(LogTemp, Warning, TEXT("Reset Combo called"))
	if (OwningCharacter)
	{
		OwningCharacter->ResetCombo();
	}
}
