// Fill out your copyright notice in the Description page of Project Settings.


#include "Characters/HanPlayerCharacter.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "EnhancedInputSubsystems.h"
#include "EnhancedInputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Components/BoxComponent.h"
#include "AnimInstances\HanAnimInstance.h"
#include "Kismet/KismetSystemLibrary.h"

AHanPlayerCharacter::AHanPlayerCharacter()
{
	GetCapsuleComponent()->InitCapsuleSize(42.f, 95.f);

	// Do not move character only rotate camera
	bUseControllerRotationPitch = false;
	bUseControllerRotationRoll = false;
	bUseControllerRotationYaw = false;

	GetCharacterMovement()->bOrientRotationToMovement = true;

	// Set up Camera Boom and Follow Camera
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(GetRootComponent());
	CameraBoom->TargetArmLength = 450.f;
	CameraBoom->bUsePawnControlRotation = true; // rotate springarm only.

	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom);
	FollowCamera->bUsePawnControlRotation = false; // do not rotate camera itself.

	// Create Weapon trace properties
	WeaponBox = CreateDefaultSubobject<UBoxComponent>(TEXT("WeaponBox"));
	WeaponBox->SetupAttachment(GetRootComponent());
	WeaponBox->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetIncludingScale, FName("weapon_box"));
	
	BoxTraceStart = CreateDefaultSubobject<USceneComponent>(TEXT("BoxTraceStart"));
	BoxTraceStart->SetupAttachment(WeaponBox);

	BoxTraceEnd = CreateDefaultSubobject<USceneComponent>(TEXT("BoxTraceEnd"));
	BoxTraceEnd->SetupAttachment(WeaponBox);
}

void AHanPlayerCharacter::BeginPlay()
{
	Super::BeginPlay();
	
	if (APlayerController* PlayerController = Cast<APlayerController>(Controller))
	{
		if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer()))
		{
			Subsystem->AddMappingContext(DefaultMappingContext, 0);
		}
	}

	WeaponBox->OnComponentBeginOverlap.AddDynamic(this, &AHanPlayerCharacter::OnBoxBeginOverlap);
}

void AHanPlayerCharacter::OnBoxBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	UE_LOG(LogTemp, Warning, TEXT("Weapon box begin overlap!!"));
	FHitResult BoxHit;

	BoxTrace(BoxHit);
}

void AHanPlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{

	if (UEnhancedInputComponent* EnhancedInputComponent = CastChecked<UEnhancedInputComponent>(PlayerInputComponent))
	{
		EnhancedInputComponent->BindAction(JumpAction, ETriggerEvent::Started, this, &ACharacter::Jump);
		EnhancedInputComponent->BindAction(MoveAction, ETriggerEvent::Triggered, this, &AHanPlayerCharacter::Move);
		EnhancedInputComponent->BindAction(LookAction, ETriggerEvent::Triggered, this, &AHanPlayerCharacter::Look);
		EnhancedInputComponent->BindAction(AttackAction, ETriggerEvent::Started, this, &AHanPlayerCharacter::AttackInputAction);
	}
}

void AHanPlayerCharacter::Move(const FInputActionValue& value)
{
	FVector2D MoveValue = value.Get<FVector2D>();

	if (Controller != nullptr)
	{
		// which is forward.
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation = FRotator(0.f, Rotation.Yaw, 0.f);

		// Get forward vector
		const FVector ForwardVector = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);

		// Get right vector 
		const FVector RightVector = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);

		AddMovementInput(ForwardVector, MoveValue.X);
		AddMovementInput(RightVector, MoveValue.Y);
	}
}

void AHanPlayerCharacter::Look(const FInputActionValue& value)
{
	FVector2D LookVector = value.Get<FVector2D>();

	if (Controller != nullptr)
	{
		AddControllerYawInput(LookVector.X);
		AddControllerPitchInput(LookVector.Y);
	}
}

void AHanPlayerCharacter::AttackInputAction(const FInputActionValue& value)
{
	Attack();
}

void AHanPlayerCharacter::Attack()
{
	// UE_LOG(LogTemp, Warning, TEXT("Attacking"));
	if (bIsAttacking)
	{
		bSaveAttack = true;
	}
	else
	{
		bIsAttacking = true;
		PerformComboAttack();
	}
}

void AHanPlayerCharacter::PerformComboAttack()
{
	UHanAnimInstance* AnimInstance = Cast<UHanAnimInstance>(GetMesh()->GetAnimInstance());
	if (AnimInstance && IsValidAttackMontages())
	{
		switch (AttackCount)
		{
		case 0:
			AttackCount = 1;
			AnimInstance->Montage_Play(AttackMontage0);
			UE_LOG(LogTemp, Warning, TEXT("Attack Count 0"))
				break;
		case 1:
			AttackCount = 2;
			AnimInstance->Montage_Play(AttackMontage1);
			UE_LOG(LogTemp, Warning, TEXT("Attack Count 1"))
				break;
		case 2:
			AttackCount = 3;
			AnimInstance->Montage_Play(AttackMontage2);
			UE_LOG(LogTemp, Warning, TEXT("Attack Count 2"))
				break;
		case 3:
			AttackCount = 0;
			AnimInstance->Montage_Play(AttackMontage3);
			UE_LOG(LogTemp, Warning, TEXT("Attack Count 3"))
				break;
		default:
			UE_LOG(LogTemp, Error, TEXT("Invalid Attack Count"))
				break;
		}
	}
}

void AHanPlayerCharacter::BoxTrace(FHitResult& OutBoxHit)
{
	FVector Start = BoxTraceStart->GetComponentLocation();
	FVector End = BoxTraceEnd->GetComponentLocation();

	TArray<AActor*> ActorToIgnored;
	ActorToIgnored.Add(this);

	for (AActor* Actor : ActorToIgnored)
	{
		ActorToIgnored.Add(Actor);
	}

	UKismetSystemLibrary::BoxTraceSingle(
		this,
		Start,
		End,
		BoxTraceExtent,
		BoxTraceStart->GetComponentRotation(),
		ETraceTypeQuery::TraceTypeQuery1,
		false,
		ActorToIgnored,
		EDrawDebugTrace::ForDuration,
		OutBoxHit,
		true
	);
	IgnoreActors.AddUnique(OutBoxHit.GetActor());
}

void AHanPlayerCharacter::ComboAttackSave()
{
	// UE_LOG(LogTemp, Warning, TEXT("Combo Attack Save Called!!"));
	if (bSaveAttack)
	{
		bSaveAttack = false;
		PerformComboAttack();
	}
}

void AHanPlayerCharacter::ResetCombo()
{
	// UE_LOG(LogTemp, Warning, TEXT("Reset Combo Called!!"));
	AttackCount = 0;
	bIsAttacking = false;
	bSaveAttack = false;
}

const bool AHanPlayerCharacter::IsValidAttackMontages() const
{
	return AttackMontage0 && AttackMontage1 && AttackMontage2 && AttackMontage3;
}

