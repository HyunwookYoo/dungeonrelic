// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "InputActionValue.h"
#include "HanPlayerCharacter.generated.h"

class UInputMappingContext;
class UInputAction;
class USpringArmComponent;
class UCameraComponent;
class UAnimMontage;
class UBoxComponent;

UCLASS()
class DUNGEONRELIC_API AHanPlayerCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	AHanPlayerCharacter();

protected:
	virtual void BeginPlay() override;

	UFUNCTION()
	void OnBoxBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

public:	
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

private:
	/* Input Action Functions */
	void Move(const FInputActionValue& value);

	void Look(const FInputActionValue& value);

	void AttackInputAction(const FInputActionValue& value);

	void Attack();

	void PerformComboAttack();

	/*( Combat Helper Functions */
	void BoxTrace(FHitResult& OutBoxHit);

public:
	void ComboAttackSave();
	
	void ResetCombo();

private:
	/* Camera Components */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Camera", meta = (AllowPrivateAccess = "true"))
	USpringArmComponent* CameraBoom;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Camera", meta = (AllowPrivateAccess = "true"))
	UCameraComponent* FollowCamera;

	/* Inputs */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Input", meta = (AllowPrivateAccess = "true"))
	UInputMappingContext* DefaultMappingContext;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Input", meta = (AllowPrivateAccess = "true"))
	UInputAction* JumpAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Input", meta = (AllowPrivateAccess = "true"))
	UInputAction* MoveAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Input", meta = (AllowPrivateAccess = "true"))
	UInputAction* LookAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Input", meta = (AllowPrivateAccess = "true"))
	UInputAction* AttackAction;

	/* Combat varaibles */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Combat", meta = (AllowPrivateAccess = "true"))
	bool bIsAttacking;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Combat", meta = (AllowPrivateAccess = "true"))
	bool bSaveAttack;

	UPROPERTY(VisibleAnywhere, Category = "Combat")
	int AttackCount = 0;

	UPROPERTY(VisibleAnywhere, Category = "Combat")
	UBoxComponent* WeaponBox;

	UPROPERTY(EditAnywhere, Category = "Combat")
	FVector BoxTraceExtent = FVector(5.f);

	UPROPERTY(VisibleAnywhere)
	USceneComponent* BoxTraceStart;

	UPROPERTY(VisibleAnywhere)
	USceneComponent* BoxTraceEnd;

	TArray<AActor*> IgnoreActors;

	/* Animation Montages */
	UPROPERTY(EditDefaultsOnly, Category = "Combat | Montages")
	UAnimMontage* AttackMontage0;

	UPROPERTY(EditDefaultsOnly, Category = "Combat | Montages")
	UAnimMontage* AttackMontage1;

	UPROPERTY(EditDefaultsOnly, Category = "Combat | Montages")
	UAnimMontage* AttackMontage2;

	UPROPERTY(EditDefaultsOnly, Category = "Combat | Montages")
	UAnimMontage* AttackMontage3;

private:
	const bool IsValidAttackMontages() const;
};
