// Copyright Epic Games, Inc. All Rights Reserved.

#include "DungeonRelic.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, DungeonRelic, "DungeonRelic" );
