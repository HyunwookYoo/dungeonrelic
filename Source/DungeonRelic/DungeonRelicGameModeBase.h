// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "DungeonRelicGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class DUNGEONRELIC_API ADungeonRelicGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
